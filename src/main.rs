use serde::{Deserialize, Serialize};
use serde_json::Result as JsonResult;
use std::{fs, collections::HashMap};
use std::io::Write;

#[derive(Serialize, Deserialize, Debug)]
struct ElementData {
    uid: u32,
    greenstoneWeight: f32,
    waterGuardian: String,
    vulcanicCave: i32,
    skyRiddle: String,
}

fn validate_weight(weight: f32) -> Option<&'static str> {
    [(weight >= 13.0 && weight <= 113.0, "Greenstone weight: outside the range"),
        (weight as i32 % 2 == 0, "Greenstone weight: not even")]
        .iter()
        .filter_map(|&(cond, msg)| if !cond { Some(msg) } else { None })
        .next()
}

fn validate_guardian(name: &str) -> Option<&'static str> {
    [(name.len() >= 3 && name.len() <= 15, "Water guardian name: outside the range"),
        (name.chars().next().map_or(false, |c| c.is_uppercase()), "Water guardian name: not capitalised"),
        (name.matches('a').count() == 2, "Water guardian name: missing 'a'")]
        .iter()
        .filter_map(|&(cond, msg)| if !cond { Some(msg) } else { None })
        .next()
}

fn validate_temperature(temp: i32) -> Option<&'static str> {
    [(temp >= 400 && temp <= 700, "Vulcanic cave temperature: outside the range"),
        (temp % 2 == 0, "Vulcanic cave temperature: not even")]
        .iter()
        .filter_map(|&(cond, msg)| if !cond { Some(msg) } else { None })
        .next()
}

fn validate_riddle(riddle: &str) -> Option<&'static str> {
    [(riddle.starts_with(char::is_uppercase), "Sky riddle: not capitalised"),
        (riddle.ends_with('.'), "Sky riddle: missing dot"),
        (riddle.chars().filter(|c| "aeiou".contains(*c)).count() % 2 == 0, "Sky riddle: not even")]
        .iter()
        .filter_map(|&(cond, msg)| if !cond { Some(msg) } else { None })
        .next()
}
fn main() -> JsonResult<()> {
    let file_content = fs::read_to_string("test-data.json").expect("Error reading file");
    let elements: Vec<ElementData> = serde_json::from_str(&file_content)?;

    // Prepare to write to a file
    let mut file = fs::File::create("validation_results.txt").expect("Unable to create file");

    for element in elements {
        let errors = vec![
            validate_weight(element.greenstoneWeight),
            validate_guardian(&element.waterGuardian),
            validate_temperature(element.vulcanicCave),
            validate_riddle(&element.skyRiddle),
        ];

        // Filter out None values and collect errors, if any
        let error_messages: Vec<&str> = errors.into_iter().filter_map(|e| e).collect();

        // Determine if the element is valid or list its errors
        let validation_result = if error_messages.is_empty() {
            "Valid".to_string()
        } else {
            error_messages.join(", ")
        };

        // Write the results to the file
        writeln!(file, "UID {}: {}", element.uid, validation_result).expect("Unable to write to file");
    }

    println!("Validation results have been written to validation_results.txt");

    Ok(())
}
