# Lab9 - Atlantis task (Rust)



----------------
### Prelim. info

*Crates used for this program:*

- serde_derive
- serde_json


# The task: The Lost Treasure of Atlantis

The task is to create a modular program that reads a json and evaluates it according to the rules provided:
#### Earth Validation
> - A number between 13-113
> - An even number
#### Water Validation
> - Contains exactly two lower-case 'a' characters
> - Starts with capital letter
> - Is within 3 and 15 characters long
#### Fire Validation
> - A number between 400-700
> - An even number
#### Air Validation
> - Contains even number of lower case vowels
> - Ends with a dot

--------------
### Functions implemented:

#### validate_weight(weight: f32) -> Option<&'static str>
Function to validate "Earth" by running through the checklist
#### validate_guardian(name: &str) -> Option<&'static str>
Function to validate "Water" by running through the checklist
#### validate_temperature(temp: i32) -> Option<&'static str>
Function to validate "Fire" by running through the checklist
#### validate_riddle(riddle: &str) -> Option<&'static str>
Function to validate "Air" by running through the checklist


### Output when program is run:
```
UID 1: Water guardian name: missing 'a'
UID 11: Greenstone weight: outside the range, Water guardian name: not capitalised, Vulcanic cave temperature: outside the range, Sky riddle: not capitalised
UID 3: Water guardian name: missing 'a'
UID 44: Greenstone weight: not even, Water guardian name: missing 'a', Vulcanic cave temperature: outside the range, Sky riddle: missing dot
UID 5: Water guardian name: missing 'a', Sky riddle: not capitalised
UID 12: Water guardian name: missing 'a', Sky riddle: missing dot
UID 7: Greenstone weight: outside the range, Water guardian name: missing 'a', Sky riddle: missing dot
UID 13: Valid
UID 9: Water guardian name: not capitalised, Sky riddle: not even
UID 10: Valid
```